/*
Navicat MySQL Data Transfer

Source Server         : app
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : apicloud_im

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-11-21 11:31:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for apicloud_im_userrelative
-- ----------------------------
DROP TABLE IF EXISTS `apicloud_im_userrelative`;
CREATE TABLE `apicloud_im_userrelative` (
  `AutoID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `UserID` int(10) unsigned NOT NULL COMMENT '用户ID',
  `UserRelativeID` int(10) unsigned NOT NULL COMMENT '用户关系ID',
  `ConversationType` varchar(50) NOT NULL COMMENT '会话类型',
  `CreateTime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`AutoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apicloud_im_userrelative
-- ----------------------------
