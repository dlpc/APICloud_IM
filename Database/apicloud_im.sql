/*
Navicat MySQL Data Transfer

Source Server         : linshiren.gotoftp3.com_3306
Source Server Version : 50622
Source Host           : linshiren.gotoftp3.com:3306
Source Database       : linshiren

Target Server Type    : MYSQL
Target Server Version : 50622
File Encoding         : 65001

Date: 2015-11-21 14:58:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for apicloud_im_user
-- ----------------------------
DROP TABLE IF EXISTS `apicloud_im_user`;
CREATE TABLE `apicloud_im_user` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `UserName` varchar(100) NOT NULL COMMENT '用户名',
  `UserPhoto` varchar(255) NOT NULL COMMENT '用户头像',
  `ConversationType` varchar(50) NOT NULL COMMENT '会话类型',
  `UserPassword` varchar(50) NOT NULL COMMENT '用户密码',
  `Token` varchar(255) NOT NULL COMMENT '融云token',
  `Regtime` int(11) NOT NULL COMMENT '注册时间',
  `CreateUserID` int(11) DEFAULT '0' COMMENT '创建人',
  `OtherID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apicloud_im_user
-- ----------------------------
INSERT INTO `apicloud_im_user` VALUES ('45', '新生帝', './Public/Uploads/2015-11-21/b7ca6f38-4ca0-cb59-cb52-012d07464467.jpg', 'PRIVATE', '55eee658d0720645e4ef9d2b91f6b571', 'Km3D2Xot6CgQdnC7h7+O4PF4KALpLwL9k7/AFWCw3AckYX1px68zx140ENXwLT6TH8WxJqpwRMI=', '1448088659', '0', '');

-- ----------------------------
-- Table structure for apicloud_im_userrelative
-- ----------------------------
DROP TABLE IF EXISTS `apicloud_im_userrelative`;
CREATE TABLE `apicloud_im_userrelative` (
  `AutoID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `UserID` int(10) unsigned NOT NULL COMMENT '用户ID',
  `UserRelativeID` int(10) unsigned NOT NULL COMMENT '用户关系ID',
  `ConversationType` varchar(50) NOT NULL COMMENT '会话类型',
  `CreateTime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`AutoID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apicloud_im_userrelative
-- ----------------------------

-- ----------------------------
-- Table structure for wt_mood
-- ----------------------------
DROP TABLE IF EXISTS `wt_mood`;
CREATE TABLE `wt_mood` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pushtime` int(11) DEFAULT NULL,
  `usysmodel` varchar(50) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `imgs` varchar(1000) DEFAULT NULL,
  `nicecount` int(11) DEFAULT '0',
  `badcount` int(11) DEFAULT '0',
  `reviewcount` int(11) DEFAULT '0',
  `sharecount` int(11) DEFAULT '0',
  `cid` int(11) DEFAULT NULL,
  `is_show_position` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wt_mood
-- ----------------------------
INSERT INTO `wt_mood` VALUES ('1', '1', '哈哈哈哈哈哈', '1447403401', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/304f9788-59ba-939c-2db7-d73e7d3712d9.jpg;./Public/Uploads/2015-11-13/91b1053d-164b-17ab-f39c-5b8a545179fd.jpg;./Public/Uploads/2015-11-13/afe8273f-4777-49ee-ebed-422cb7d0ed94.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('2', '1', '哈哈哈哈哈哈方法', '1447403490', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/304f9788-59ba-939c-2db7-d73e7d3712d9.jpg;./Public/Uploads/2015-11-13/91b1053d-164b-17ab-f39c-5b8a545179fd.jpg;./Public/Uploads/2015-11-13/afe8273f-4777-49ee-ebed-422cb7d0ed94.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('3', '1', '哈哈哈哈哈哈方法', '1447403519', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('4', '1', '自己咔咔咔解决设计师', '1447403524', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('5', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403529', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('6', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403539', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('7', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403545', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('8', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403548', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('9', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403552', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('10', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403554', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('11', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403560', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('12', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403563', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('13', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403567', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('14', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403570', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('15', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403573', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('16', '1', '自己咔咔咔解决设计师还在纠结啊啊', '1447403575', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c68f7849-dbfe-1c3b-7fe0-3a0428e686ca.jpg;./Public/Uploads/2015-11-13/52336cde-9201-2061-97d9-9e26821a2eae.jpg;./Public/Uploads/2015-11-13/c02908fa-6c4a-c644-e702-0567bbd094dc.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('17', '1', '从CV VV', '1447404118', 'leo', '广东省中山市中山市市辖区中山二路48号', '', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('18', '1', '哈哈哈哈哈哈，很无聊', '1447408940', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/b71adb89-fd3d-ba9b-d645-22fc2b3bbefa.jpg;./Public/Uploads/2015-11-13/c6ac858c-e0d0-9e1d-1c4f-ef7216c5e886.jpg;./Public/Uploads/2015-11-13/775058ea-515f-0948-c353-a237f35e7304.jpg;./Public/Uploads/2015-11-13/bf81bfb1-f8bd-c3f2-a26a-fb74f08642c1.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('19', '1', '啵啵啵啵啵呵呵', '1447409959', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/c75a8017-8aba-9b29-07c8-32a082e4a3e5.jpg;./Public/Uploads/2015-11-13/ee376f4c-dc31-21ac-bc7c-ee8bb594bf38.jpg;./Public/Uploads/2015-11-13/7cd8cf57-03ba-aef7-b928-af52f42c2323.jpg;./Public/Uploads/2015-11-13/f4c9545c-aae0-c375-2948-a2b6b98f0235.jpg;./Public/Uploads/2015-11-13/2e688dc7-6246-8515-080f-b50fe8a9606d.jpg;./Public/Uploads/2015-11-13/a3d31745-0ab8-f177-4e14-47063e0fd7fc.jpg;', '0', '0', '0', '0', '1', '0', '1');
INSERT INTO `wt_mood` VALUES ('20', '1', 'VV VV BB油条', '1447410933', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/149c9e09-6389-6658-2532-c3a4f2b370bc.jpg;./Public/Uploads/2015-11-13/7eb927ad-7d33-c8d4-6589-4e1ee0d36227.jpg;./Public/Uploads/2015-11-13/1b8d839a-afc0-28b4-e5b2-69ae3bf5de59.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('21', '1', '了了了了了了了力量', '1447411800', 'leo', '广东省中山市中山市市辖区中山二路48号', './Public/Uploads/2015-11-13/641d0f75-bdd1-ba0e-fa3c-20f7f8fa607d.jpg;./Public/Uploads/2015-11-13/93b222d9-77e0-cbd6-3aee-9e582a06de24.jpg;./Public/Uploads/2015-11-13/9aed835a-374b-685d-552c-bd9ec5526966.jpg;./Public/Uploads/2015-11-13/57078c1f-c743-1756-c1c6-6d7ffbe1decd.jpg;./Public/Uploads/2015-11-13/0f615efa-2fa7-3de3-b6a6-f05a14eb811a.jpg;./Public/Uploads/2015-11-13/ce0cd192-316b-b7a2-4134-6edd22e60154.jpg;', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('22', '1', '啦啦啦啦啦啦', '1447412185', 'leo', '广东省中山市中山市市辖区中山二路48号', '', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('23', '4', '雨后哈哈哈哈哈哈', '1447428171', 'iPhone 6s', '广东省中山市中山市市辖区豪源街13号', '', '0', '0', '0', '0', '1', '1', '1');
INSERT INTO `wt_mood` VALUES ('24', '1', 'CV VV VV VV BB VV VV BB', '1447428247', 'leo', '广东省中山市中山市市辖区豪源街13号', '', '0', '0', '0', '0', '1', '1', '1');

-- ----------------------------
-- Table structure for wt_user
-- ----------------------------
DROP TABLE IF EXISTS `wt_user`;
CREATE TABLE `wt_user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) NOT NULL,
  `upwd` char(32) NOT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `phone` varchar(30) NOT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `gender` varchar(8) NOT NULL,
  `birthday` date NOT NULL,
  `height` float unsigned NOT NULL,
  `width` float unsigned NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `qq` varchar(15) DEFAULT NULL,
  `wx` varchar(100) DEFAULT NULL,
  `wb` varchar(100) DEFAULT NULL,
  `regtime` int(11) NOT NULL,
  `level` varchar(50) NOT NULL,
  `levelcount` int(10) unsigned NOT NULL,
  `carecount` int(11) DEFAULT NULL,
  `fanscount` int(11) DEFAULT NULL,
  `dutycount` int(11) DEFAULT NULL,
  `recordcount` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wt_user
-- ----------------------------
INSERT INTO `wt_user` VALUES ('1', '新生帝', '55eee658d0720645e4ef9d2b91f6b571', './Public/Uploads/2015-11-13/p-497287ca.jpg', '18676265646', '大家好，我叫新生帝', '男', '1992-05-27', '174.5', '67', null, null, null, null, '1446659135', '匀称一哥', '1', '0', '0', '0', '0');
INSERT INTO `wt_user` VALUES ('2', '新生后', '55eee658d0720645e4ef9d2b91f6b571', null, '15521512520', '广告歌', '男', '2015-11-11', '175.5', '63', null, null, null, null, '1447233688', '匀称一哥', '1', '0', '0', '0', '0');
INSERT INTO `wt_user` VALUES ('3', 'Peter', 'e10adc3949ba59abbe56e057f20f883e', null, '13600339493', '33', '男', '1988-02-01', '173', '65', null, null, null, null, '1447234147', '匀称一哥', '1', '0', '0', '0', '0');
INSERT INTO `wt_user` VALUES ('4', 'bam', '87a84dba19d4c64dbd72d5469cb8c962', null, '18680186668', '你好！', '男', '1985-11-22', '175', '62', null, null, null, null, '1447428112', '匀称一哥', '1', '0', '0', '0', '0');
INSERT INTO `wt_user` VALUES ('5', '关毅杰Chris', '1573c46fd82c808cc0461a9a947f4c9a', null, '18680188333', '歪理兔', '男', '1982-01-09', '172', '52', null, null, null, null, '1447428131', '匀称一哥', '1', '0', '0', '0', '0');
INSERT INTO `wt_user` VALUES ('6', 'rickywen', '43e6a52e7541730c92cb703a4f8646fd', null, '18620623117', 'PT', '男', '1982-01-17', '170', '75', null, null, null, null, '1447428135', '匀称一哥', '1', '0', '0', '0', '0');
INSERT INTO `wt_user` VALUES ('7', '肉紧一号', 'b853f98f394239ac15ea6a3d908122a7', null, '18933381313', '大家好', '男', '2015-10-11', '166', '130', null, null, null, null, '1447428137', '匀称一哥', '1', '0', '0', '0', '0');
INSERT INTO `wt_user` VALUES ('8', 'Ray', 'c8837b23ff8aaa8a2dde915473ce0991', './Public/Uploads/2015-11-14/AEACDCE7-244F-4636-BD0A-C3607EB14FA2-1685-00000203609BADA5.jpg', '13927886080', '秦皮', '男', '1988-10-07', '170', '60', null, null, null, null, '1447428254', '匀称一哥', '1', '0', '0', '0', '0');
INSERT INTO `wt_user` VALUES ('9', '33', '5958b78867a9db27dade6adc88e3857d', null, '18676233533', '哈哈', '女', '1992-02-23', '158', '47', null, null, null, null, '1447428308', '匀称一哥', '1', '0', '0', '0', '0');
